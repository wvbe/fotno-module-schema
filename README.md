# fontoxml-development-tools-module-schema

The Development tools for FontoXML can support a developer when configuring a FontoXML editor.
This module contains commands for showing information about the schema.

## Installation

This module is part of the FontoXML development tools and is not meant to be used separately. To install the FontoXML development tools, run the following command:

	npm i -g @fontoxml/fontoxml-development-tools

### Usage examples

	fdt attributes [--schema <schemaPath>] [--columns name ns use defaultvalue ...]

Output a table with information about all attributes in a schema.

	fdt attribute <attributeName> [--schema <schemaPath>]

Output schema information about a specific attribute. An attribute may have different definitions in the same schema, this command summarizes them all.

	fdt elements [--schema <schemaPath>] [--columns name ns local desc atts ...]

Output a table with information about all elements in a schema.

	fdt element <elementName> [--schema <schemaPath>]

Output schema information about a specific element, including detailed content model info.

	fdt schemas [--columns package locations path]

Output a table with information about all schemas.

	fdt simpletypes [--columns name variety format]

Output a table with information about all simple types in a schema.
