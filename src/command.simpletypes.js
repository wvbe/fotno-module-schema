'use strict';

const Table = require('@fontoxml/fontoxml-development-tools-module-core').TableCommand;

const getClosestMatchingSchemaSummary = require('./api/getClosestMatchingSchemaSummary');

module.exports = fotno => {
	const table = new Table(fotno, [
		{
			name: 'name',
			default: true,
			label: 'Name',
			value: (element) => element.localName
		},
		{
			name: 'variety',
			default: true,
			label: 'Variety',
			value: (element) => element.variety
		},
		{
			name: 'format',
			default: true,
			label: 'Format',
			value: (element) => element.getSimpleTypeString()
		}
	]);

	function simpleTypesCommand (req, res) {
		res.caption(req.command.getLongName());

		req.fdt.editorRepository.throwIfNotInsideEditorRepository();

		const destroySpinner = res.spinner('Looking up schemas...');

		const schemaSummary = getClosestMatchingSchemaSummary(req.fdt.editorRepository.path, req.options.schema);

		destroySpinner();
		res.break();

		table.print(
			res,
			req.options.columns,
			schemaSummary.getAllSimpleTypes(),
			req.options.sort,
			req.options.export
		);
	}

	fotno.registerCommand('simpletypes', simpleTypesCommand)
		.addAlias('list-simpletypes')
		.setDescription('Output a table with information about all simple types in a schema.')

		.addOption('schema', 's', 'In a multi-schema app, specify (a part of) the path to the source schema.json.', false)
		.addOption(table.sortOption)
		.addOption(table.columnsOption)
		.addOption(table.exportOption)

		.addExample(`${fotno.getAppInfo().name} simpletypes`, 'Output information about all the simple types if the application has only one schema.json.')
		.addExample(`${fotno.getAppInfo().name} simpletypes -s map`, 'Output information about all the simple types if the application has one shell in a package named something with "map".')
		.addExample(`${fotno.getAppInfo().name} simpletypes -s packages/non-standard-schema-package/src/data.json`, 'Output information about all the simple types from a schema.json in a non-standard location.');
};
