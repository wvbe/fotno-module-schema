'use strict';

const Table = require('@fontoxml/fontoxml-development-tools-module-core').TableCommand;

const getClosestMatchingSchemaSummary = require('./api/getClosestMatchingSchemaSummary');

module.exports = fotno => {
	const table = new Table(fotno, [
		{
			name: 'name',
			default: true,
			label: 'Name',
			value: (element) => element.localName
		},
		{
			name: 'ns',
			default: true,
			label: 'Namespace',
			value: (element) => element.getNameSpace()
		},
		{
			name: 'local',
			default: true,
			label: 'Local',
			value: (element) => element.isLocal
		},
		{
			name: 'desc',
			default: true,
			label: 'Description',
			value: (element) => element.documentation
		},
		{
			name: 'cnt',
			label: 'Content model',
			value: (element) => element.getContentModelString()
		},
		{
			name: 'empty',
			label: 'Empty',
			value: (element) => element.isEmpty()
		},
		{
			name: 'mixed',
			label: 'Mixed',
			value: (element) => element.isMixed
		},
		{
			name: 'abstract',
			label: 'Abstract',
			value: (element) => element.isAbstract
		},
		{
			name: 'children',
			label: 'Contains',
			value: (element) => element.getChildElements().map(e => e.localName).join(', ')
		},
		{
			name: 'parents',
			label: 'Contained by',
			value: (element) => element.getParentElements().map(e => e.localName).join(', ')
		},
		{
			name: 'attrs',
			label: 'Attributes',
			value: (element) => element.getAttributes().map(attr => attr.localName + (attr.use === 'required' ? '*' : '')).join(', ')
		},
		{
			name: 'simpletype',
			label: 'Simple type',
			value: (element) => {
				const simpleType = element.getSimpleType();
				return simpleType ? simpleType.getSimpleTypeString() : null;
			}
		},
		{
			name: 'class',
			label: 'Class',
			value: (element) => element.getAttributes()
				.filter(attr => attr.localName === 'class')
				.map(attr => attr.defaultValue)
				.filter(classAttr => !!classAttr).join(', ')
		}
	]);

	function elementCommand (req, res) {
		res.caption(req.command.getLongName());

		req.fdt.editorRepository.throwIfNotInsideEditorRepository();

		const destroySpinner = res.spinner('Looking up schemas...');

		const schemaSummary = getClosestMatchingSchemaSummary(req.fdt.editorRepository.path, req.options.schema);

		destroySpinner();
		res.break();

		table.print(
			res,
			req.options.columns,
			schemaSummary.getAllElements()
				.filter(element => {
					if (!req.options['blacklist-ns'].length) {
						return true;
					}

					const ns = element.getNameSpace();

					if (!ns) {
						return true;
					}

					return req.options['blacklist-ns'].indexOf(ns) === -1;
				}),
			req.options.sort,
			req.options.export
		);
	}

	fotno.registerCommand('elements', elementCommand)
		.addAlias('list-elements')
		.setDescription('Output a table with information about all elements in a schema.')

		.addOption('schema', 's', 'In a multi-schema app, specify (a part of) the path to the source schema.json.', false)
		.addOption(new fotno.MultiOption('blacklist-ns')
			.setDescription('Do not list elements of this namespace, eg "mml".')
		)
		.addOption(table.sortOption)
		.addOption(table.columnsOption)
		.addOption(table.exportOption)

		.addExample(`${fotno.getAppInfo().name} elements`, 'Output information about all the elements if the application has only one schema.json.')
		.addExample(`${fotno.getAppInfo().name} elements -s map`, 'Output information about all the elements if the application has one shell in a package named something with "map".')
		.addExample(`${fotno.getAppInfo().name} elements -s packages/non-standard-schema-package/src/data.json`, 'Output information about all the elements from a schema.json in a non-standard location.')
		.addExample(`${fotno.getAppInfo().name} elements --blacklist-ns xml`, 'Output information about all the elements, but do not output elements from the xml namespace.');
};
