'use strict';

const Table = require('@fontoxml/fontoxml-development-tools-module-core').TableCommand;

const getClosestMatchingSchemaSummary = require('./api/getClosestMatchingSchemaSummary');

module.exports = fotno => {
	const table = new Table(fotno, [
		{
			name: 'name',
			default: true,
			label: 'Name',
			value: (attribute) => attribute.localName
		},
		{
			name: 'ns',
			default: true,
			label: 'Namespace',
			value: (attribute) => attribute.getNameSpace()
		},
		{
			name: 'use',
			default: true,
			label: 'Use',
			value: (attribute) => attribute.use
		},
		{
			name: 'defaultvalue',
			default: true,
			label: 'Default value',
			value: (attribute) => attribute.defaultValue
		},
		{
			name: 'allowedValues',
			label: 'Allowed values',
			value: (attribute) => attribute.allowedValues ? attribute.allowedValues.join(', ') : null
		},
		{
			name: 'typename',
			label: 'Type name',
			value: (attribute) => attribute.typeLocalName
		},
		{
			name: 'typenamespace',
			label: 'Type namespace',
			value: (attribute) => attribute.typeNamespace
		},
		{
			name: 'simpletype',
			label: 'Simple type',
			value: (element) => {
				const simpleType = element.getSimpleType();
				return simpleType ? simpleType.getSimpleTypeString() : null;
			}
		}
	]);

	function attributesCommand (req, res) {
		res.caption(req.command.getLongName());

		req.fdt.editorRepository.throwIfNotInsideEditorRepository();

		const destroySpinner = res.spinner('Looking up schemas...');

		const schemaSummary = getClosestMatchingSchemaSummary(req.fdt.editorRepository.path, req.options.schema);

		destroySpinner();
		res.break();

		table.print(
			res,
			req.options.columns,
			schemaSummary.getAllAttributes()
				.filter(attribute => {
					if (!req.options['blacklist-ns'].length) {
						return true;
					}

					const ns = attribute.getNameSpace();

					if (!ns) {
						return true;
					}

					return req.options['blacklist-ns'].indexOf(ns) === -1;
				}),
			req.options.sort,
			req.options.export
		);
	}

	fotno.registerCommand('attributes', attributesCommand)
		.addAlias('list-attributes')
		.setDescription('Output a table with information about all attributes in a schema.')

		.addOption('schema', 's', 'In a multi-schema app, specify (a part of) the path to the source schema.json.', false)
		.addOption(new fotno.MultiOption('blacklist-ns')
			.setDescription('Do not list attributes of this namespace, eg "mml".')
		)
		.addOption(table.sortOption)
		.addOption(table.columnsOption)
		.addOption(table.exportOption)

		.addExample(`${fotno.getAppInfo().name} attributes`, 'Output information about all the attributes if the application has only one schema.json.')
		.addExample(`${fotno.getAppInfo().name} attributes -s map`, 'Output information about all the attributes if the application has one shell in a package named something with "map".')
		.addExample(`${fotno.getAppInfo().name} attributes -s packages/non-standard-schema-package/src/data.json`, 'Output information about all the attributes from a schema.json in a non-standard location.')
		.addExample(`${fotno.getAppInfo().name} attributes --blacklist-ns xml`, 'Output information about all the attributes, but do not output attributes from the xml namespace.');
};
