'use strict';

const getClosestMatchingSchemaSummary = require('./api/getClosestMatchingSchemaSummary');
const renderSimpleType = require('./helpers/renderSimpleType');

module.exports = (fotno) => {
	function attributeCommand (req, res) {
		res.caption(req.command.getLongName());

		req.fdt.editorRepository.throwIfNotInsideEditorRepository();

		const destroySpinner = res.spinner('Looking up schemas...');

		const schemaSummary = getClosestMatchingSchemaSummary(req.fdt.editorRepository.path, req.options.schema);
		let attributeName = null;
		let elementName = null;

		if (req.parameters.attribute.indexOf('@') >= 0) {
			elementName = req.parameters.attribute.split('@')[0];
			attributeName = req.parameters.attribute.split('@')[1];
		}
		else {
			attributeName = req.parameters.attribute;
		}

		let attributeSummaries = schemaSummary.getAllAttributes()
			.filter(attr => attr.localName === attributeName);

		destroySpinner();

		res.debug('Reading from ' + schemaSummary.package);
		res.debug(`Found ${attributeSummaries.length} attribute definitions for attribute "${attributeName}" across ${attributeSummaries.reduce((tot, sum) => tot + sum.getElements().length, 0)} attributes.`);

		if (elementName) {
			res.debug(`Filtering definitions applicable to <${elementName}>`);

			attributeSummaries = attributeSummaries
				.filter(attributeSummary => attributeSummary.getElements().some(elementSummary => elementSummary.localName === elementName));
		}

		if (!attributeSummaries.length) {
			throw new fotno.InputError(
				`Looks like there is no "${attributeName}" attribute definition that matches your criteria.`,
				'Check your input for typos');
		}

		attributeSummaries.forEach(function (attributeSummary, index) {
			res.caption('Definition ' + (index + 1));

			res.indent();

			res.properties({
				'localName': attributeSummary.localName,
				'Type localName': attributeSummary.typeLocalName,
				'Type namespace': attributeSummary.typeNamespace || '-',
				'Use': attributeSummary.use || '-',
				'Default value': attributeSummary.defaultValue || '-',
				'Allowed values': attributeSummary.allowedValues.length ?
					attributeSummary.allowedValues.join(', ') :
					'-',
				'Elements': attributeSummary.getElements().length ?
					attributeSummary.getElements().map(elementSummary => elementSummary.localName).join(', ') :
					'-'
			});

			const attributeSimpleType = attributeSummary.getSimpleType();
			// Old schemas do not have simpleTypes, so skip this entire section if it is not used in this schema
			if (attributeSimpleType) {
				res.debug('Simple type');
				res.indent();
				renderSimpleType(res, attributeSimpleType);
				res.outdent();
			}

			res.outdent();
		});
	}

	fotno.registerCommand('attribute', attributeCommand)
		.setDescription('Output schema information about a specific attribute. An attribute may have different definitions in the same schema, this command summarizes them all.')
		.setLongDescription('An attribute may have different definitions per schema.json because an attribute of the same name may have different valid options, defaults or data type in other elements.')

		.addParameter('attribute', 'The name of an attribute. Use either attributeName or elementName@attributeName.', true)
		.addOption('schema', 's', 'Specify (a part of) the path to the source schema.json.', false)

		.addExample(`${fotno.getAppInfo().name} attribute id`, 'Output information about all attribute definitions for @id if the application has only one schema.json.')
		.addExample(`${fotno.getAppInfo().name} attribute id -s map`, 'Output information about all attribute definitions for "id" if the application has one shell in a package named something with "map".')
		.addExample(`${fotno.getAppInfo().name} attribute ol@class -s topic`, 'Output information about the @class attribute on <ol> in a DITA shell matching "topic".');
};
