'use strict';

const getClosestMatchingSchemaSummary = require('./api/getClosestMatchingSchemaSummary');
const renderSimpleType = require('./helpers/renderSimpleType');

function stringForEmptyOrMixed (empty, mixed) {
	return mixed ?
		(empty ? 'Text only (mixed, empty)' : 'Text and elements (mixed)') :
		(empty ? 'Nothing (empty)' : 'Elements only');
}

function percentualize (ratio) {
	return (Math.round(ratio * 1000) / 10) + '%';
}

module.exports = fotno => {
	function elementCommand (req, res) {
		res.caption(req.command.getLongName());

		req.fdt.editorRepository.throwIfNotInsideEditorRepository();

		const destroySpinner = res.spinner('Looking up schemas...');

		const schemaSummary = getClosestMatchingSchemaSummary(req.fdt.editorRepository.path, req.options.schema);
		const allElementSummaries = schemaSummary.getAllElements();
		const elementSummaries = allElementSummaries
				.filter(elementSummary => {
					return elementSummary.localName === req.parameters.element;
				});

		destroySpinner();

		res.debug('Reading from ' + schemaSummary.package);
		res.debug(`Found ${elementSummaries.length} element definitions for "${req.parameters.element}" across ${allElementSummaries.length} elements.`);

		if (!elementSummaries.length) {
			res.break();
			res.notice('No elements found, stopping.');
			return;
		}

		elementSummaries
			.forEach((elementSummary, index) => {

				res.caption('Definition ' + (index + 1));

				res.indent();

				const contentModelString = elementSummary.getContentModelString();
				const childElements = elementSummary.getChildElements();
				const parentElements = elementSummary.getParentElements();
				const attributes = elementSummary.getAttributes();

				res.properties({
					'Element name': elementSummary.localName + (elementSummary.isLocal ? ' (local)' : ''),
					'Documentation': elementSummary.documentation || '-',
					'Contents': stringForEmptyOrMixed(elementSummary.isEmpty(), elementSummary.isMixed),
					'Contained by': parentElements.length + ' unique nodes (' + percentualize(parentElements.length / allElementSummaries.length) + ')',
					'Contains': childElements.length + ' unique nodes (' + percentualize(childElements.length / allElementSummaries.length) + ')',
					'Attributes': elementSummary.getAttributes().length + ' unique attributes',
					'Default class': elementSummary.getAttributes()
						.filter(attr => attr.localName === 'class')
						.map(attr => attr.defaultValue)
						.filter(classAttr => !!classAttr).join(', ') || '-'
				});

				const elementSimpleType = elementSummary.getSimpleType();
				if (elementSimpleType) {
					res.debug('Simple type');
					res.indent();
					renderSimpleType(res, elementSimpleType);
					res.outdent();
				}

				if (contentModelString) {
					res.caption('Content model');
					res.debug(contentModelString);
				}

				if (childElements.length) {
					res.caption('Contains...');
					res.debug(childElements.map(e => e.localName).join(', '));
				}

				if (parentElements.length) {
					res.caption('Contained by...');
					res.debug(parentElements.map(e => e.localName).join(', '));
				}

				if (attributes.length) {
					res.caption('Attributes...');
					res.debug(attributes.map(attr => attr.localName + (attr.use === 'required' ? '*' : '')).join(', '));
				}

				res.outdent();
			});
	}

	fotno.registerCommand('element', elementCommand)
		.setDescription('Output schema information about a specific element, including detailed content model info.')

		.addParameter('element', 'Query a specific element.', true)
		.addOption('schema', 's', 'Specify (a part of) the path to the source schema.json.', false)

		.addExample(`${fotno.getAppInfo().name} element article`, 'Output information about the <article> element if the application has only one schema.json.')
		.addExample(`${fotno.getAppInfo().name} element topicref -s map`, 'Output information about the <topicref> element if the application has one shell in a package named something with "map".')
		.addExample(`${fotno.getAppInfo().name} element body -s packages/non-standard-schema-package/src/data.json`, 'Output information about the <body> element from a schema.json in a non-standard location.');
};
