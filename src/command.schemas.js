'use strict';

const Table = require('@fontoxml/fontoxml-development-tools-module-core').TableCommand;

const getSchemaSummaries = require('./api/getSchemaSummaries');

module.exports = (fotno) => {
	const table = new Table(fotno, [
		{
			name: 'package',
			default: true,
			label: 'Package',
			value: (schema) => schema.package
		},
		{
			name: 'locations',
			default: true,
			label: 'Locations',
			value: (schema) => schema.locations.join(', ')
		},
		{
			name: 'path',
			default: true,
			label: 'Path',
			value: (schema) => schema.path
		}
	]);

	function schemasCommand (req, res) {
		res.caption(req.command.getLongName());

		req.fdt.editorRepository.throwIfNotInsideEditorRepository();

		const destroySpinner = res.spinner('Looking up schemas...');

		const schemaSummaries = getSchemaSummaries(req.fdt.editorRepository.path);

		destroySpinner();
		res.break();

		table.print(
			res,
			req.options.columns,
			schemaSummaries,
			req.options.sort,
			req.options.export
		);
	}

	fotno.registerCommand('schemas', schemasCommand)
		.addAlias('list-schemas')
		.setDescription('Output a table with information about all schemas.')

		.addOption(table.sortOption)
		.addOption(table.columnsOption)
		.addOption(table.exportOption)

		.addExample(`${fotno.getAppInfo().name} schemas`, 'Output a list of all schemas in the application.');
};
