'use strict';

const formatterPerRestrictionType = {
		enumeration: value => value.join('\n')
		// fractionDigits,
		// length,
		// maxExclusive,
		// maxInclusive,
		// maxLength,
		// minExclusive,
		// minInclusive
		// minLength,
		// pattern,
		// totalDigits,
		// whiteSpace
	};

function formatType (restrictionType) {
	restrictionType = restrictionType.replace(/([A-Z])/g, ' $1');
	return restrictionType.charAt(0).toUpperCase() + restrictionType.substr(1);
}

function formatValue (restrictionType, value) {
	return formatterPerRestrictionType[restrictionType] ?
		formatterPerRestrictionType[restrictionType](value) :
		value;
}

module.exports = {
	formatType,
	formatValue
};
