'use strict';

const restrictionFormatter = require('./restrictionFormatter');

const keyLength = 14;

module.exports = function renderSimpleType (res, simpleType) {
	if (simpleType.isBuiltIn()) {
		res.property(
			'Built-in:',
			simpleType.localName || '-',
			keyLength
		);
	}
	else if (simpleType.isDerived()) {
		res.log('Derived:');
		res.indent();
		const key = Object.keys(simpleType.restrictions)
				.map(restrictionFormatter.formatType)
				.join('\n');
		const value = Object.keys(simpleType.restrictions)
				.map(restrictionType => restrictionFormatter.formatValue(restrictionType, simpleType.restrictions[restrictionType]))
				.join('\n');
		res.property(
			key || '-',
			value || '-',
			keyLength);

		const base = simpleType.getBase();
		if (base) {
			res.log('Base');
			res.indent();
			renderSimpleType(res, base);
			res.outdent();
		}

		res.outdent();
	}
	else if (simpleType.isList()) {
		res.log('List:');
		res.indent();
		renderSimpleType(res, simpleType.getItemType());
		res.outdent();
	}
	else if (simpleType.isUnion()) {
		const memberTypes = simpleType.getMemberTypes();
		res.log(`Union: (${memberTypes.length} member types)`);
		res.indent();
		memberTypes.forEach((simpleType) => {
			renderSimpleType(res, simpleType);
		});
		res.outdent();
	}
	else {
		res.log(`Unknown simple type "${simpleType.variety}"`);
	}
};
