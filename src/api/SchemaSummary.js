'use strict';

const path = require('path');
const requireJs = require('requirejs');

const AttributeSummary = require('./AttributeSummary');
const ElementSummary = require('./ElementSummary');
const SimpleTypeSummary = require('./SimpleTypeSummary');

function findConfigurationSettings (instance) {
	const pathPieces = instance.path.split(path.sep);
	const packageRootIndex = pathPieces.findIndex(piece => ['packages', 'packages-shared', 'platform', 'platform-linked'].indexOf(piece) >= 0);

	instance.locations = [];

	if (packageRootIndex === -1) {
		// throw new Error('Schema is not in a packages directory');
		return;
	}

	const applicationPath = path.resolve.apply(undefined, ['/'].concat(pathPieces.slice(0, packageRootIndex)));
	const rjs = requireJs({
			paths: {
				text: path.resolve(applicationPath, 'platform', 'fontoxml-vendor-requirejs-text', 'text'),
				json: path.resolve(applicationPath, 'platform', 'fontoxml-vendor-requirejs-plugins', 'src', 'json')
			}
		});

	instance.package = pathPieces[packageRootIndex + 1];

	try {
		const schemaLocationsPath = path.resolve.apply(undefined, ['/'].concat(pathPieces.slice(0, packageRootIndex + 2)).concat(['src', 'SCHEMA_LOCATIONS.js']));
		instance.locations = rjs(path.resolve(applicationPath, schemaLocationsPath));
	}
	catch (_error) {
		// instance.locations = [];
	}
}

const ATTRIBUTES = Symbol();
const DATA = Symbol();
const ELEMENTS = Symbol();
const LOCAL_ELEMENTS = Symbol();
const SIMPLETYPES = Symbol();

module.exports = class SchemaSummary {
	constructor (schemaPath) {
		this.path = schemaPath;
		this.package = null;
		this.locations = [];

		findConfigurationSettings(this);

		this[DATA] = require(this.path);

		if (!this[DATA].localElements) {
			this[DATA].localElements = [];
		}

		this[DATA].simpleTypes = this[DATA].simpleTypes
			.map((simpleType, i) => Object.assign(simpleType, {
				index: i
			}));

		this[DATA].elements = this[DATA].elements
			.map((element, i) => Object.assign(element, {
				isLocal: false,
				index: i
			}));

		this[DATA].localElements = this[DATA].localElements
			.map((element, i) => Object.assign(element, {
				isLocal: true,
				index: i
			}));

		this[DATA].attributes = this[DATA].attributes
			.map((attribute, i) => Object.assign(attribute, {
				index: i
			}));

		this[DATA].contentModels = this[DATA].contentModels
			.map((contentModel, i) => Object.assign(contentModel, {
				index: i
			}));

		this[ATTRIBUTES] = [];
		this[ELEMENTS] = [];
		this[LOCAL_ELEMENTS] = [];
		this[SIMPLETYPES] = [];
	}

	getSimpleTypeByIndex (i) {
		if (i === undefined) {
			// simpleTypes do not exist on schemas prior to SDK 6.9 or 6.8,
			// so make an exception for "empty simpleTypeRef"
			return;
		}

		if (!this[SIMPLETYPES][i]) {
			this[SIMPLETYPES][i] = new SimpleTypeSummary(this, this[DATA].simpleTypes[i]);
		}

		return this[SIMPLETYPES][i];
	}

	getAllSimpleTypes () {
		return this[DATA].simpleTypes.map(simpleType => this.getSimpleTypeByIndex(simpleType.index));
	}

	getElementByIndex (i) {
		if (!this[ELEMENTS][i]) {
			this[ELEMENTS][i] = new ElementSummary(this, this[DATA].elements[i]);
		}

		return this[ELEMENTS][i];
	}

	getLocalElementByIndex (i) {
		if (!this[LOCAL_ELEMENTS][i]) {
			this[LOCAL_ELEMENTS][i] = new ElementSummary(this, this[DATA].localElements[i]);
		}

		return this[LOCAL_ELEMENTS][i];
	}

	getAllElements () {
		return this.getAllGlobalElements().concat(this.getAllLocalElements());
	}

	getAllGlobalElements () {
		return this[DATA].elements.map(element => this.getElementByIndex(element.index));
	}

	getAllLocalElements () {
		return this[DATA].localElements.map(element => this.getLocalElementByIndex(element.index));
	}

	getAttributeByIndex (i) {
		if (!this[ATTRIBUTES][i]) {
			this[ATTRIBUTES][i] = new AttributeSummary(this, this[DATA].attributes[i]);
		}

		return this[ATTRIBUTES][i];
	}

	getAllAttributes () {
		return this[DATA].attributes.map(attribute => this.getAttributeByIndex(attribute.index));
	}

	getContentModelByIndex (i) {
		return this[DATA].contentModels[i];
	}

	getAllContentModels () {
		return this[DATA].contentModels.map(contentModel => this.getContentModelByIndex(contentModel.index));
	}

	matchesSearchString (str) {
		if (this.package.indexOf(str) >= 0) {
			return true;
		}

		if (this.locations.some(location => location.indexOf(str) >= 0)) {
			return true;
		}

		return false;
	}
};
