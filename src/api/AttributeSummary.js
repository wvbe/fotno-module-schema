'use strict';

const INDEX = Symbol();
const SCHEMAFILE = Symbol();
const SIMPLETYPE_REF = Symbol();

module.exports = class AttributeSummary {
	constructor (schemaFile, attributeDefinition) {
		this[SCHEMAFILE] = schemaFile;

		this[INDEX] = attributeDefinition.index;

		this[SIMPLETYPE_REF] = attributeDefinition.simpleTypeRef;

		this.localName = attributeDefinition.localName;
		this.typeLocalName = attributeDefinition.typeLocalName;
		this.typeNamespace = attributeDefinition.typeNamespace;
		this.use = attributeDefinition.use;
		this.defaultValue = attributeDefinition.defaultValue;
		this.allowedValues = (attributeDefinition.allowedValues || []).sort();
	}

	getElements () {
		// @TODO Note said this was 'BROKEN', check why this was marked as broken
		return this[SCHEMAFILE].getAllElements()
			.filter(elementSummary => elementSummary.refersToAttributeIndex(this[INDEX]))
			.sort((a, b) => a.localName.localeCompare(b.localName));
	}

	getNameSpace () {
		return this.localName.indexOf(':') === -1 ?
			null :
			this.localName.split(':')[0];
	}

	getSimpleType () {
		return this[SCHEMAFILE].getSimpleTypeByIndex(this[SIMPLETYPE_REF]);
	}
};
