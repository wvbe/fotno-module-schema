'use strict';

const restrictionFormatter = require('../helpers/restrictionFormatter');

const BASE = Symbol();
const ITEM_TYPE = Symbol();
const MEMBER_TYPES = Symbol();
const SCHEMAFILE = Symbol();

module.exports = class SimpleTypeSummary {
	constructor (schemaFile, simpleTypeDefinition) {
		this[SCHEMAFILE] = schemaFile;

		this[BASE] = simpleTypeDefinition.base;
		this[ITEM_TYPE] = simpleTypeDefinition.itemType;
		this[MEMBER_TYPES] = simpleTypeDefinition.memberTypes || [];

		// Built-in primitives: string, boolean, decimal, float, double, duration, dateTime, time, date, gYearMonth,
		//   gYear, gMonthDay, gDay, gMonth, hexBinary, base64Binary, anyURI, QNam, NOTATION
		// Build in derived: normalizedString, token, language, NMTOKEN, NMTOKENS, Name, NCName, ID, IDREF, IDREFS,
		//   ENTITY, ENTITIES, integer, nonPositiveInteger, negativeInteger, long, int, short, byte, nonNegativeInteger,
		//   unsignedLong, unsignedInt, unsignedShort, unsignedByte, positiveInteger
		this.localName = simpleTypeDefinition.localName;

		// builtin, derived, list, union
		this.variety = simpleTypeDefinition.variety;

		// Save this 1-1, not sure what to do with the different properties:
		// - enumeration
		// - pattern
		this.restrictions = simpleTypeDefinition.restrictions;
	}

	isBuiltIn () {
		return this.variety === 'builtin';
	}

	isDerived () {
		return this.variety === 'derived';
	}

	isList () {
		return this.variety === 'list';
	}

	isUnion () {
		return this.variety === 'union';
	}

	getBase () {
		if (this[BASE] === undefined) {
			return null;
		}

		return this[SCHEMAFILE].getSimpleTypeByIndex(this[BASE]);
	}

	getItemType () {
		return this[SCHEMAFILE].getSimpleTypeByIndex(this[ITEM_TYPE]);
	}

	getMemberTypes () {
		return this[MEMBER_TYPES].map(index => this[SCHEMAFILE].getSimpleTypeByIndex(index));
	}

	getSimpleTypeString () {
		switch (this.variety) {
			case 'builtin': {
				return this.localName;
			}
			case 'derived': {
				const base = this.getBase();
				const restrictions = this.restrictions;
				return 'Derived ' +
					(Object.keys(restrictions).length ? '{ ' : '') +
					Object.keys(restrictions).map(restriction => {
						return restrictionFormatter.formatType(restriction) +
							': ' +
							restrictionFormatter.formatValue(restriction, restrictions[restriction])
								.toString()
								.replace(/\n/g, ' | ');
					}).join(', ') +
					(Object.keys(restrictions).length ? ' }' : '') +
					' ( ' + (base ? base.getSimpleTypeString() : 'Unkown base') + ' )';
			}
			case 'list': {
				const itemType = this.getItemType();
				return 'List ( ' + (itemType ? itemType.getSimpleTypeString() : 'Unknown item type') + ' )';
			}
			case 'union': {
				const memberTypes = this.getMemberTypes();
				if (!memberTypes.length) {
					return 'Union [ ]';
				}
				return 'Union [ (' +
					memberTypes.map(simpleType => simpleType.getSimpleTypeString()).join(' ), ( ') +
					' ) ]';
			}
			default: {
				return `Unknown simple type "${this.variety}"`;
			}
		}
	}
};
