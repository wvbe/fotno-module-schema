'use strict';

const fs = require('fs'),
	path = require('path');

const SchemaSummary = require('./SchemaSummary');
const getSchemaSummaries = require('./getSchemaSummaries');

module.exports = function getClosestMatchingSchema (p, searchString) {
	try {
		fs.statSync(path.resolve(process.cwd(), searchString));
		return new SchemaSummary(path.resolve(process.cwd(), searchString));
	}
	catch (_error) {
		// searchString is not an existing file
	}

	const schemaLocations = getSchemaSummaries(p);
	const results = (schemaLocations.length === 1 || !searchString) ?
			schemaLocations :
			schemaLocations.filter(schemaLocation => schemaLocation.matchesSearchString(searchString));

	if (!schemaLocations.length) {
		throw new Error(
			'Couldn\'t find a schema file in this application.\n' +
			'Use the schema compiler on http://sdk.fontoxml.com to obtain the optimized schema JSON file for your XSD\'s, or type the full path to the schema JSON relative to your current working directory.'
		);
	}

	const shorthandSchemaLocations = schemaLocations.map(sL => sL.package);

	if (results.length < 1) {
		throw new Error(
			`Couldn't find a schema matching with "${searchString}"\n` +
			`Try to find an unambiguous short for one of the following:\n\t- ${shorthandSchemaLocations.join('\n\t- ')}\nOr type the full path to the schema JSON relative to your current working directory.`
		);
	}

	if (results.length > 1) {
		if (searchString) {
			throw new Error(
				`The string "${searchString}" yielded more than one results.\n` +
				`Try to find an unambiguous short for one of the following:\n\t- ${shorthandSchemaLocations.join('\n\t- ')}\nOr type the full path to the schema JSON relative to your current working directory.`
			);
		}
		else {
			throw new Error(
				'This application has more than one schema, you must use the --schema option to specify (a part of) the package name.\n' +
				`Try to find an unambiguous short for one of the following:\n\t- ${shorthandSchemaLocations.join('\n\t- ')}\nOr type the full path to the schema JSON relative to your current working directory.`
			);
		}
	}

	return results[0];
};
