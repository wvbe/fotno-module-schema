'use strict';

const ATTRIBUTE_REFS = Symbol();
const CONTENTMODEL_REF = Symbol();
const INDEX = Symbol();
const SCHEMAFILE = Symbol();
const SIMPLETYPE_REF = Symbol();

function flattenContentModel (schemaFile, contentModel) {
	return contentModel.items ?
		contentModel.items.reduce(function (accum, item) {
			return accum.concat(flattenContentModel(schemaFile, item));
		}, []) :
		[contentModel];
}

function transformContentModel (schemaFile, contentModel) {
	const cardinality = contentModel.minOccurs === contentModel.maxOccurs ?
		(contentModel.minOccurs === 1 ? '' : '{' + contentModel.minOccurs + '}') :
		'{' + contentModel.minOccurs + ',' + (contentModel.maxOccurs || '…') + '}';

	switch (contentModel.type) {
		case 'localElement': {
			const localElement = schemaFile.getLocalElementByIndex(contentModel.elementRef);
			return './' + localElement.localName + cardinality;
		}

		case 'element': {
			const element = schemaFile.getAllGlobalElements().find(e => e.localName === contentModel.localName);
			return element.localName + cardinality;
		}

		case 'sequence': {
			return '(' + contentModel.items
					.map(transformContentModel.bind(undefined, schemaFile))
					.join(' ') + ')' + cardinality;
		}

		case 'choice': {
			return '(' + contentModel.items
					.map(transformContentModel.bind(undefined, schemaFile))
					.join('|') + ')' + cardinality;
		}
	}
}

function getElementsReferringToElement (schemaFile, elementSummary) {
	const contentModels = schemaFile.getAllContentModels()
		.filter(function filterContentModel (contentModel) {
			if (contentModel.items) {
				return contentModel.items.some(filterContentModel);
			}

			return elementSummary.isLocal ?
				contentModel.type === 'localElement' && contentModel.elementRef === elementSummary[INDEX] :
				contentModel.type === 'element' && contentModel.localName === elementSummary.localName;
		});

	return schemaFile.getAllElements().filter(parentSummary => {
		return contentModels.some(cm => parentSummary[CONTENTMODEL_REF] === cm.index);
	});
}

module.exports = class ElementSummary {
	constructor (schemaFile, elementDefinition) {
		this[SCHEMAFILE] = schemaFile;

		this[INDEX] = elementDefinition.index;

		this[ATTRIBUTE_REFS] = elementDefinition.attributeRefs || [];
		this[CONTENTMODEL_REF] = elementDefinition.contentModelRef;
		this[SIMPLETYPE_REF] = elementDefinition.simpleTypeRef;

		this.isLocal = elementDefinition.isLocal;
		this.localName = elementDefinition.localName;
		this.isMixed = !!elementDefinition.isMixed;
		this.isAbstract = !!elementDefinition.isAbstract;
		this.documentation = elementDefinition.documentation ?
			elementDefinition.documentation.replace(/\s\s+/g, ' ') :
			null;
	}

	getContentModel () {
		return this[SCHEMAFILE].getContentModelByIndex(this[CONTENTMODEL_REF]);
	}

	getSimpleType () {
		return this[SCHEMAFILE].getSimpleTypeByIndex(this[SIMPLETYPE_REF]);
	}

	isEmpty () {
		return !this.getChildElements().length;
	}

	refersToAttributeIndex (attributeIndex) {
		return this[ATTRIBUTE_REFS].indexOf(attributeIndex) >= 0;
	}

	getAttributes () {
		return this[ATTRIBUTE_REFS].map(attributeRef => this[SCHEMAFILE].getAttributeByIndex(attributeRef))
			.sort((a, b) => a.localName.localeCompare(b.localName));
	}

	getChildElements () {
		return flattenContentModel(this[SCHEMAFILE], this.getContentModel())
			.map(contentModel => {
				if (contentModel.type === 'localElement') {
					return this[SCHEMAFILE].getLocalElementByIndex(contentModel.elementRef);
				}

				if (contentModel.type === 'element') {
					return this[SCHEMAFILE].getAllGlobalElements().find(e => e.localName === contentModel.localName);
				}
			})
			.filter(element => !!element)
			.sort((a, b) => a.localName.localeCompare(b.localName));
	}

	getParentElements () {
		return getElementsReferringToElement(this[SCHEMAFILE], this)
			.sort((a, b) => a.localName.localeCompare(b.localName));
	}

	getContentModelString () {
		return transformContentModel(this[SCHEMAFILE], this.getContentModel());
	}

	getNameSpace () {
		return this.localName.indexOf(':') === -1 ?
			null :
			this.localName.split(':')[0];
	}
};
