'use strict';

const glob = require('globby');
const path = require('path');

const SchemaSummary = require('./SchemaSummary');

module.exports = function getSchemaSummaries (p) {
	const basePath = path.resolve(p);

	return glob.sync([
			// Traditional schema files compiled in a build
			'packages/**/schema.json',
			'packages-shared/**/schema.json',

			// Lazy loaded schema files
			'packages/**/assets/schemas/*.json',
			'packages-shared/**/assets/schemas/*.json'
		], { cwd: basePath })
		.filter(function (val, i, self) {
			const packageName = val.split('/').slice(0, 2).join('/');
			return self.findIndex(otherVal => packageName === otherVal.split('/').slice(0, 2).join('/')) === i;
		})
		.map(schemaPath => new SchemaSummary(path.join(basePath, schemaPath)));
};
