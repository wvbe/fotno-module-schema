'use strict';

const getSchemaSummaries = require('./src/api/getSchemaSummaries');

module.exports = fotno => {
	fotno.registerContextInformer((req, res) => {
		res.caption('Schema locations');

		const schemaSummaries = req.fdt.editorRepository.path ? getSchemaSummaries(req.fdt.editorRepository.path) : [];

		if (!schemaSummaries.length) {
			res.debug('No schemas found.');
		}

		schemaSummaries.forEach(schemaSummary => res.definition(
			schemaSummary.package,
			schemaSummary.locations.join('\n') || 'No schema location'));
	});

	[
		require('./src/command.attribute.js'),
		require('./src/command.attributes.js'),
		require('./src/command.element.js'),
		require('./src/command.elements.js'),
		require('./src/command.schemas.js'),
		require('./src/command.simpletypes.js')
	].forEach(mod => mod(fotno));
};

module.exports.getClosestMatchingSchemaSummary = require('./src/api/getClosestMatchingSchemaSummary');
module.exports.getSchemaSummaries = require('./src/api/getSchemaSummaries');

module.exports.AttributeSummary = require('./src/api/AttributeSummary');
module.exports.ElementSummary = require('./src/api/ElementSummary');
module.exports.SchemaSummary = require('./src/api/SchemaSummary');
